package com.example.cloud.ui.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.cloud.R;
import com.example.cloud.databinding.FragmentTempBinding;
import com.jake.mvvm.base.BaseFragment;
import com.jake.mvvm.base.NoViewModel;

public class FindFragment extends BaseFragment<NoViewModel, FragmentTempBinding> {

    @Override
    public int setContent() {
        return R.layout.fragment_temp;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showContentView();
    }


}
