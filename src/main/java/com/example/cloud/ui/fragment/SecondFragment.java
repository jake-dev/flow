package com.example.cloud.ui.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cloud.R;
import com.example.cloud.databinding.FragmentContentBinding;
import com.example.cloud.ui.adapter.ContentFragmentAdapter;
import com.jake.mvvm.base.BaseFragment;
import com.jake.mvvm.base.NoViewModel;

import java.util.ArrayList;
import java.util.List;

public class SecondFragment extends BaseFragment<NoViewModel, FragmentContentBinding> {

    @Override
    public int setContent() {
        return R.layout.fragment_content;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showContentView();
        initViewPager();
    }

    private void initViewPager() {
        List fragments = initFragment();
        bindingView.vpContent.setAdapter(new ContentFragmentAdapter(getChildFragmentManager(),fragments));
        bindingView.vpContent.setOffscreenPageLimit(2);
        bindingView.tabLayout.setupWithViewPager(bindingView.vpContent);
    }

    private List initFragment() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new TempFragment());
        return fragments;
    }

}
