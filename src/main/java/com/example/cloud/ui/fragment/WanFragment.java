package com.example.cloud.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.cloud.R;
import com.example.cloud.databinding.FragmentWanBinding;
import com.example.cloud.databinding.FragmentWanHeaderBinding;
import com.example.cloud.databinding.ItemBannerWanBinding;
import com.example.cloud.ui.adapter.WanAdapter;
import com.example.cloud.ui.bean.WanBannerItemBean;
import com.example.cloud.ui.model.WanViewModel;
import com.example.cloud.utils.GlideUtil;
import com.jake.mvvm.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import me.jingbin.library.ByRecyclerView;
import me.jingbin.library.adapter.BaseByViewHolder;
import me.jingbin.library.adapter.BaseRecyclerAdapter;
import me.jingbin.library.decoration.SpacesItemDecoration;
import me.jingbin.sbanner.config.BannerConfig;
import me.jingbin.sbanner.config.ScaleRightTransformer;
import me.jingbin.sbanner.holder.HolderCreator;
import me.jingbin.sbanner.holder.SBannerViewHolder;

public class WanFragment extends BaseFragment<WanViewModel, FragmentWanBinding> {

    private FragmentWanHeaderBinding rvHeader;
    private WanAdapter rvAdapter;
    private int page;
    private boolean mIsFirst;

    @Override
    public int setContent() {
        return R.layout.fragment_wan;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showContentView();
        initView();
        initRecyleView();
        requestBanner();
    }

    private void requestBanner() {
        viewModel.getWanBannerItemBeanMutableLiveData().observe(getViewLifecycleOwner(), new Observer<WanBannerItemBean>() {
            @Override
            public void onChanged(WanBannerItemBean wanBannerItemBean) {
                if (wanBannerItemBean == null) {
                    rvHeader.banner.setVisibility(View.GONE);
                } else {
                    showBanner(wanBannerItemBean.getData());
                }
            }
        });
    }

    private void showBanner(List<WanBannerItemBean.DataBean> bannerList) {
        rvHeader.banner.setIndicatorRes(R.drawable.banner_red, R.drawable.banner_grey)
                .setBannerStyle(BannerConfig.NOT_INDICATOR)
                .setBannerAnimation(ScaleRightTransformer.class)
                .setDelayTime(6000)
                .setOffscreenPageLimit(bannerList.size())
                .setAutoPlay(true)
                .setPages(bannerList, new HolderCreator<SBannerViewHolder>() {
                    @Override
                    public SBannerViewHolder createViewHolder() {

                        return new SBannerViewHolder<WanBannerItemBean.DataBean>() {
                            private ItemBannerWanBinding itemBannerWanBinding;

                            @Override
                            public View createView(Context context) {
                                itemBannerWanBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.item_banner_wan, null, false);
                                return itemBannerWanBinding.getRoot();
                            }

                            @Override
                            public void onBind(Context context, int i, WanBannerItemBean.DataBean wanBannerItemBean) {
                                GlideUtil.displayImage(wanBannerItemBean.getImagePath(), itemBannerWanBinding.iv);
                            }
                        };
                    }

                }).start();
    }

    private void initRecyleView() {
    }

    private void initView() {
        rvHeader = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_wan_header, null, false);
        bindingView.rv.addHeaderView(rvHeader.getRoot());

        initAdapter();


    }


    private List<WanBannerItemBean> getMore(){
        final List<WanBannerItemBean> list = new ArrayList<>();
        list.add(new WanBannerItemBean());
        return list;
    }


    private void initAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        bindingView.rv.setLayoutManager(layoutManager);
        SpacesItemDecoration itemDecoration = new SpacesItemDecoration(bindingView.rv.getContext(), SpacesItemDecoration.VERTICAL);
        itemDecoration.setDrawable(ContextCompat.getDrawable(bindingView.rv.getContext(), R.drawable.shape_line));
        bindingView.rv.addItemDecoration(itemDecoration);
        rvAdapter = new WanAdapter(R.layout.item_wan);
        bindingView.rv.setAdapter(rvAdapter);
        bindingView.rv.setOnLoadMoreListener(new ByRecyclerView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                bindingView.rv.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (page == 3) {
                            bindingView.rv.loadMoreEnd();
                            return;
                        }
                        if (page == 1) {
                            page++;
                            bindingView.rv.loadMoreFail();
                            return;
                        }
                        page++;
                        rvAdapter.addData(getMore());
                        bindingView.rv.loadMoreComplete();
                    }
                }, 500);
            }
        });
        bindingView.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!bindingView.rv.isLoadingMore()) {
                    bindingView.rv.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bindingView.swipeRefreshLayout.setRefreshing(false);
                            page = 1;
                            rvAdapter.setNewData(getMore());
                        }
                    }, 500);
                } else {
                    bindingView.swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
        mIsFirst = false;
    }


}
