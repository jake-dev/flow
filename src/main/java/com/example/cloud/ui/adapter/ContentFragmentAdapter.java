package com.example.cloud.ui.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

public class ContentFragmentAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragmentList = null;
    private List<String> titleList = null;
    public ContentFragmentAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    public ContentFragmentAdapter(FragmentManager supportFragmentManager, List<Fragment> list) {
        this(supportFragmentManager, 0);
        this.fragmentList = list;
    }

    public ContentFragmentAdapter(FragmentManager supportFragmentManager, List<Fragment> fragments,List<String> titles) {
        this(supportFragmentManager, 0);
        this.fragmentList = fragments;
        this.titleList = titles;
    }

    @NonNull
    @Override
    public Fragment getItem(int i) {
        return fragmentList.get(i);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (titleList!=null&&position<titleList.size()){
            return titleList.get(position);
        }
        return super.getPageTitle(position);
    }
}
