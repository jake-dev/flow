package com.example.cloud.ui.adapter;

import com.example.cloud.databinding.ItemWanBinding;
import com.example.cloud.ui.bean.WanBannerItemBean;
import com.jake.mvvm.adapter.BaseBindingAdapter;

public class WanAdapter extends BaseBindingAdapter<WanBannerItemBean,ItemWanBinding> {
    public WanAdapter(int layoutId) {
        super(layoutId);
    }

    @Override
    protected void bindView(BaseBindingHolder holder, WanBannerItemBean bean, ItemWanBinding binding, int position) {
        binding.tvNew.setText(bean.toString());
    }




//    private ArrayList<WanBannerItemBean> list;
//
//    public WanAdapter(ArrayList<WanBannerItemBean> list) {
//        this.list = list;
//    }
//
//    @NonNull
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        ItemWanBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_wan, parent, false);
//        ViewHolder holder = new ViewHolder(binding.getRoot());
//        return holder;
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
//        ItemWanBinding binding = DataBindingUtil.getBinding(holder.itemView);
//        binding.tvNew.setText("测试测试测试测试测试测试测试测试测试测试");
//        binding.executePendingBindings();
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return list == null ? 0 : list.size();
//    }
//
//
//    class ViewHolder extends RecyclerView.ViewHolder {
//
//        public ViewHolder(@NonNull View itemView) {
//            super(itemView);
//        }
//    }
}
