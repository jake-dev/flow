package com.example.cloud.ui.model;

import android.app.Application;

import androidx.annotation.NonNull;

import com.jake.mvvm.base.BaseViewModel;

public class MainViewModel extends BaseViewModel {
    public MainViewModel(@NonNull Application application) {
        super(application);
    }
}
