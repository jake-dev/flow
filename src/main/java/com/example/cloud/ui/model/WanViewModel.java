package com.example.cloud.ui.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;


import com.example.cloud.repo.ApiMgr;
import com.example.cloud.ui.bean.WanBannerItemBean;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class WanViewModel extends AndroidViewModel {
    private MutableLiveData<WanBannerItemBean> wanBannerItemBeanMutableLiveData;

    public WanViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<WanBannerItemBean> getWanBannerItemBeanMutableLiveData() {
        if (wanBannerItemBeanMutableLiveData == null) {
            wanBannerItemBeanMutableLiveData = new MutableLiveData<>();
        }
        ApiMgr.getInstance().getWanApi().getWanAndroidBanner().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<WanBannerItemBean>() {
                    @Override
                    public void accept(WanBannerItemBean wanBannerItemBean) throws Exception {
                        wanBannerItemBeanMutableLiveData.setValue(wanBannerItemBean);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        wanBannerItemBeanMutableLiveData.setValue(null);
                    }
                });
        return wanBannerItemBeanMutableLiveData;
    }

}
