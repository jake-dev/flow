package com.example.cloud.ui.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import androidx.appcompat.app.ActionBar;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.cloud.R;
import com.example.cloud.config.ConstantsUrl;
import com.example.cloud.databinding.ActivityMainBinding;
import com.example.cloud.databinding.MenuMainBinding;
import com.example.cloud.ui.adapter.ContentFragmentAdapter;
import com.example.cloud.ui.fragment.FirstFragment;
import com.example.cloud.ui.fragment.SecondFragment;
import com.example.cloud.ui.fragment.ThirdFragment;
import com.example.cloud.ui.model.MainViewModel;
import com.example.cloud.utils.GlideUtil;
import com.jake.mvvm.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity<MainViewModel, ActivityMainBinding> implements View.OnClickListener {


    private MenuMainBinding menuMainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showContentView();
        initView();
        initDrawerLayout();
        initViewPage();
    }

    private void initViewPage() {
        List<Fragment> list  = initFragments();
        bindingView.iAppMain.vpContent.setAdapter(new ContentFragmentAdapter(getSupportFragmentManager(),list));
        bindingView.iAppMain.vpContent.setOffscreenPageLimit(2);
        bindingView.iAppMain.vpContent.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                setCurrentItem(i);
            }
            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        bindingView.iAppMain.ivTitleFirst.setSelected(true);
        bindingView.iAppMain.vpContent.setCurrentItem(0);
    }

    private List<Fragment> initFragments() {
        List<Fragment> list = new ArrayList<>();
        list.add(new FirstFragment());
        list.add(new SecondFragment());
        list.add(new ThirdFragment());
        return list;
    }

    private void setCurrentItem(final int i) {
        bindingView.iAppMain.vpContent.setCurrentItem(i);
        bindingView.iAppMain.ivTitleSecond.setSelected(false);
        bindingView.iAppMain.ivTitleFirst.setSelected(false);
        bindingView.iAppMain.ivTitleThird.setSelected(false);

        switch (i) {
            case 0:
                bindingView.iAppMain.ivTitleFirst.setSelected(true);
                break;
            case 1:
                bindingView.iAppMain.ivTitleSecond.setSelected(true);
                break;
            case 2:
                bindingView.iAppMain.ivTitleThird.setSelected(true);
                break;
        }
    }

    private void initDrawerLayout() {
    }

    private void initView() {
        setNoTitle();
        setSupportActionBar(bindingView.iAppMain.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }
        bindingView.nvMain.inflateHeaderView(R.layout.menu_main);
        menuMainBinding = DataBindingUtil.bind(bindingView.nvMain.getHeaderView(0));
        GlideUtil.displayCircle(menuMainBinding.ivAvatar, ConstantsUrl.IC_AVATAR);

        bindingView.iAppMain.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bindingView.drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        bindingView.iAppMain.ivTitleFirst.setOnClickListener(this);
        bindingView.iAppMain.ivTitleSecond.setOnClickListener(this);
        bindingView.iAppMain.ivTitleThird.setOnClickListener(this);


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if(keyCode == KeyEvent.KEYCODE_BACK){
            if (bindingView.drawerLayout.isDrawerOpen(GravityCompat.START)){
                bindingView.drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                moveTaskToBack(true);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_title_first:
                setCurrentItem(0);
                break;
            case R.id.iv_title_second:
                setCurrentItem(1);
                break;
            case R.id.iv_title_third:
                setCurrentItem(2);
                break;
        }
    }
}
