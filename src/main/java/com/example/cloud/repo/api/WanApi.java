package com.example.cloud.repo.api;

import com.example.cloud.ui.bean.WanBannerItemBean;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface WanApi {

    /**
     * 玩安卓轮播图
     */
    @GET("banner/json")
    Observable<WanBannerItemBean> getWanAndroidBanner();
}
