package com.example.cloud.repo;

import com.example.cloud.config.ConstantsUrl;
import com.example.cloud.repo.api.WanApi;
import com.jake.mvvm.http.HttpUtils;

public class ApiMgr {
    private WanApi wanApi;
    private static volatile ApiMgr instance;
    public static ApiMgr getInstance() {
        if (instance == null) {
            synchronized (ApiMgr.class) {
                if (instance == null) {
                    instance = new ApiMgr();
                }
            }
        }
        return instance;
    }
    public WanApi getWanApi() {
        if (wanApi == null) {
            wanApi = HttpUtils.getInstance().getBuilder(ConstantsUrl.API_WAN_ANDROID).build().create(WanApi.class);
        }
        return wanApi;
    }


}
