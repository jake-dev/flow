package com.dream.flow.util;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class ScriptEngineManagerUtil {

    public static boolean eval(String javaScript){
        ScriptEngineManager manager=new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");
        try {
            return (boolean) engine.eval(javaScript);
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        return false;
    }


}
