package com.dream.flow.dao;

import com.dream.flow.entity.Flow;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * (Flow)表数据库访问层
 *
 * @author makejava
 * @since 2019-12-01 20:52:57
 */
@Mapper
public interface FlowDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Flow queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Flow> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param flow 实例对象
     * @return 对象列表
     */
    List<Flow> queryAll(Flow flow);

    /**
     * 新增数据
     *
     * @param flow 实例对象
     * @return 影响行数
     */
    int insert(Flow flow);

    /**
     * 修改数据
     *
     * @param flow 实例对象
     * @return 影响行数
     */
    int update(Flow flow);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

}