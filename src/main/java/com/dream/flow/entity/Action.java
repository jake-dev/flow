package com.dream.flow.entity;

import java.io.Serializable;

/**
 * (Action)实体类
 *
 * @author makejava
 * @since 2019-11-30 20:50:12
 */
public class Action implements Serializable {
    private static final long serialVersionUID = 776737651255733929L;
    
    private String id;
    
    private String flowId;
    
    private String nodeId;
    
    private String handlePersonId;
    
    private String handleParams;
    
    private String createTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlowId() {
        return flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getHandlePersonId() {
        return handlePersonId;
    }

    public void setHandlePersonId(String handlePersonId) {
        this.handlePersonId = handlePersonId;
    }

    public String getHandleParams() {
        return handleParams;
    }

    public void setHandleParams(String handleParams) {
        this.handleParams = handleParams;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

}