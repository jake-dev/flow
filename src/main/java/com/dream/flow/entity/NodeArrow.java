package com.dream.flow.entity;

import java.io.Serializable;

/**
 * (NodeArrow)实体类
 *
 * @author makejava
 * @since 2019-12-01 20:49:09
 */
public class NodeArrow implements Serializable {
    private static final long serialVersionUID = 344814421937642030L;
    
    private String id;
    
    private String nodeId;
    
    private String mark;
    
    private String name;
    
    private String conditionStr;
    
    private String nextNodeId;
    
    private String ruleId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConditionStr() {
        return conditionStr;
    }

    public void setConditionStr(String conditionStr) {
        this.conditionStr = conditionStr;
    }

    public String getNextNodeId() {
        return nextNodeId;
    }

    public void setNextNodeId(String nextNodeId) {
        this.nextNodeId = nextNodeId;
    }

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

}