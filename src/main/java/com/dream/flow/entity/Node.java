package com.dream.flow.entity;

import java.io.Serializable;

/**
 * (Node)实体类
 *
 * @author makejava
 * @since 2019-11-30 21:43:25
 */
public class Node implements Serializable {
    private static final long serialVersionUID = -79922283783447680L;
    
    private String id;
    
    private String ruleId;
    
    private String name;
    /**
    * 0 开始 1 进行中 -1 归档
    */
    private Integer type;
    
    private String handleUserCondition;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getHandleUserCondition() {
        return handleUserCondition;
    }

    public void setHandleUserCondition(String handleUserCondition) {
        this.handleUserCondition = handleUserCondition;
    }

}