package com.dream.flow.entity;

import java.io.Serializable;

/**
 * (Rule)实体类
 *
 * @author makejava
 * @since 2019-11-30 12:14:59
 */
public class Rule implements Serializable {
    private static final long serialVersionUID = 815504136048073647L;
    
    private String id;
    
    private String name;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}