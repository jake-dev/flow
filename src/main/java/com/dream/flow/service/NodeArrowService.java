package com.dream.flow.service;

import com.dream.flow.entity.NodeArrow;

import java.util.List;

/**
 * (NodeArrow)表服务接口
 *
 * @author makejava
 * @since 2019-12-01 20:29:45
 */
public interface NodeArrowService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    NodeArrow queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<NodeArrow> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param nodeArrow 实例对象
     * @return 实例对象
     */
    NodeArrow insert(NodeArrow nodeArrow);

    /**
     * 修改数据
     *
     * @param nodeArrow 实例对象
     * @return 实例对象
     */
    NodeArrow update(NodeArrow nodeArrow);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    List<NodeArrow> queryByNoteId(String currentNodeId);
}