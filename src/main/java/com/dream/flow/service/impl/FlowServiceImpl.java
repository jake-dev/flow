package com.dream.flow.service.impl;

import com.dream.flow.entity.Flow;
import com.dream.flow.dao.FlowDao;
import com.dream.flow.service.FlowService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Flow)表服务实现类
 *
 * @author makejava
 * @since 2019-12-01 20:52:57
 */
@Service("flowService")
public class FlowServiceImpl implements FlowService {
    @Resource
    private FlowDao flowDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Flow queryById(String id) {
        return this.flowDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Flow> queryAllByLimit(int offset, int limit) {
        return this.flowDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param flow 实例对象
     * @return 实例对象
     */
    @Override
    public Flow insert(Flow flow) {
        this.flowDao.insert(flow);
        return flow;
    }

    /**
     * 修改数据
     *
     * @param flow 实例对象
     * @return 实例对象
     */
    @Override
    public Flow update(Flow flow) {
        this.flowDao.update(flow);
        return this.queryById(flow.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.flowDao.deleteById(id) > 0;
    }
}