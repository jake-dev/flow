package com.dream.flow.service.impl;

import com.dream.flow.dao.RuleDao;
import com.dream.flow.entity.Rule;
import com.dream.flow.service.RuleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Rule)表服务实现类
 *
 * @author makejava
 * @since 2019-11-30 12:14:59
 */
@Service("ruleService")
public class RuleServiceImpl implements RuleService {
    @Resource
    private RuleDao ruleDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Rule queryById(String id) {
        return this.ruleDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Rule> queryAllByLimit(int offset, int limit) {
        return this.ruleDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param rule 实例对象
     * @return 实例对象
     */
    @Override
    public Rule insert(Rule rule) {
        this.ruleDao.insert(rule);
        return rule;
    }

    /**
     * 修改数据
     *
     * @param rule 实例对象
     * @return 实例对象
     */
    @Override
    public Rule update(Rule rule) {
        this.ruleDao.update(rule);
        return this.queryById(rule.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.ruleDao.deleteById(id) > 0;
    }
}