package com.dream.flow.service.impl;

import com.dream.flow.dao.NodeArrowDao;
import com.dream.flow.entity.NodeArrow;
import com.dream.flow.service.NodeArrowService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (NodeArrow)表服务实现类
 *
 * @author makejava
 * @since 2019-12-01 20:29:45
 */
@Service("nodeArrowService")
public class NodeArrowServiceImpl implements NodeArrowService {
    @Resource
    private NodeArrowDao nodeArrowDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public NodeArrow queryById(String id) {
        return this.nodeArrowDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<NodeArrow> queryAllByLimit(int offset, int limit) {
        return this.nodeArrowDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param nodeArrow 实例对象
     * @return 实例对象
     */
    @Override
    public NodeArrow insert(NodeArrow nodeArrow) {
        this.nodeArrowDao.insert(nodeArrow);
        return nodeArrow;
    }

    /**
     * 修改数据
     *
     * @param nodeArrow 实例对象
     * @return 实例对象
     */
    @Override
    public NodeArrow update(NodeArrow nodeArrow) {
        this.nodeArrowDao.update(nodeArrow);
        return this.queryById(nodeArrow.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.nodeArrowDao.deleteById(id) > 0;
    }

    @Override
    public List<NodeArrow> queryByNoteId(String currentNodeId) {
        return this.nodeArrowDao.queryByNoteId(currentNodeId);
    }
}