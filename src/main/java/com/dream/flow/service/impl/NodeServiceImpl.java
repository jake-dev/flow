package com.dream.flow.service.impl;

import com.dream.flow.dao.NodeDao;
import com.dream.flow.entity.Node;
import com.dream.flow.service.NodeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Node)表服务实现类
 *
 * @author makejava
 * @since 2019-11-30 21:43:25
 */
@Service("nodeService")
public class NodeServiceImpl implements NodeService {
    @Resource
    private NodeDao nodeDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Node queryById(String id) {
        return this.nodeDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Node> queryAllByLimit(int offset, int limit) {
        return this.nodeDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param node 实例对象
     * @return 实例对象
     */
    @Override
    public Node insert(Node node) {
        this.nodeDao.insert(node);
        return node;
    }

    /**
     * 修改数据
     *
     * @param node 实例对象
     * @return 实例对象
     */
    @Override
    public Node update(Node node) {
        this.nodeDao.update(node);
        return this.queryById(node.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.nodeDao.deleteById(id) > 0;
    }

    @Override
    public Node queryByFirstNode(String ruleId) {
        return this.nodeDao.queryByFirstNode(ruleId);
    }
}