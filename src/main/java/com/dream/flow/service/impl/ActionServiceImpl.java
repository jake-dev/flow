package com.dream.flow.service.impl;

import com.dream.flow.dao.ActionDao;
import com.dream.flow.entity.Action;
import com.dream.flow.service.ActionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Action)表服务实现类
 *
 * @author makejava
 * @since 2019-11-30 20:50:12
 */
@Service("actionService")
public class ActionServiceImpl implements ActionService {
    @Resource
    private ActionDao actionDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Action queryById(String id) {
        return this.actionDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Action> queryAllByLimit(int offset, int limit) {
        return this.actionDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param action 实例对象
     * @return 实例对象
     */
    @Override
    public Action insert(Action action) {
        this.actionDao.insert(action);
        return action;
    }

    /**
     * 修改数据
     *
     * @param action 实例对象
     * @return 实例对象
     */
    @Override
    public Action update(Action action) {
        this.actionDao.update(action);
        return this.queryById(action.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.actionDao.deleteById(id) > 0;
    }
}