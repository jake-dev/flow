package com.dream.flow.service;

import com.dream.flow.entity.Rule;

import java.util.List;

/**
 * (Rule)表服务接口
 *
 * @author makejava
 * @since 2019-11-30 12:14:59
 */
public interface RuleService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Rule queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Rule> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param rule 实例对象
     * @return 实例对象
     */
    Rule insert(Rule rule);

    /**
     * 修改数据
     *
     * @param rule 实例对象
     * @return 实例对象
     */
    Rule update(Rule rule);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

}