package com.dream.flow.service;

import com.dream.flow.entity.Node;

import java.util.List;

/**
 * (Node)表服务接口
 *
 * @author makejava
 * @since 2019-11-30 21:43:25
 */
public interface NodeService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Node queryById(String id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Node> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param node 实例对象
     * @return 实例对象
     */
    Node insert(Node node);

    /**
     * 修改数据
     *
     * @param node 实例对象
     * @return 实例对象
     */
    Node update(Node node);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(String id);

    Node queryByFirstNode(String ruleId);
}