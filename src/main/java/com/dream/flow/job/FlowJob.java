package com.dream.flow.job;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class FlowJob {

    @Scheduled(cron = "0 56 20 * * ?")
    public void runfirst(){
        // 从redis获取, 待是你刚才
    }
}
