package com.dream.flow.vo;

import java.io.Serializable;

/**
 * (Flow)实体类
 *
 * @author makejava
 * @since 2019-12-01 20:52:57
 */
public class FlowVo implements Serializable {
    private static final long serialVersionUID = 496222563429642255L;
    
    private String id;
    
    private String ruleId;
    
    private String name;
    
    private String handlePersonId;
    
    private String currentNodeId;

    private String handleParams;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHandlePersonId() {
        return handlePersonId;
    }

    public void setHandlePersonId(String handlePersonId) {
        this.handlePersonId = handlePersonId;
    }

    public String getCurrentNodeId() {
        return currentNodeId;
    }

    public void setCurrentNodeId(String currentNodeId) {
        this.currentNodeId = currentNodeId;
    }

    public String getHandleParams() {
        return handleParams;
    }

    public void setHandleParams(String handleParams) {
        this.handleParams = handleParams;
    }
}