package com.dream.flow.controller;

import com.alibaba.fastjson.JSON;
import com.dream.flow.entity.Action;
import com.dream.flow.entity.Flow;
import com.dream.flow.entity.Node;
import com.dream.flow.entity.NodeArrow;
import com.dream.flow.handle.CustomException;
import com.dream.flow.handle.ReturnCode;
import com.dream.flow.service.ActionService;
import com.dream.flow.service.FlowService;
import com.dream.flow.service.NodeArrowService;
import com.dream.flow.service.NodeService;
import com.dream.flow.util.DateUtil;
import com.dream.flow.util.ScriptEngineManagerUtil;
import com.dream.flow.util.UUIDUtil;
import com.dream.flow.vo.ActionVo;
import com.dream.flow.vo.FlowVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * (Arrow)表控制层
 *
 * @author makejava
 * @since 2019-11-30 08:29:19
 */
@RestController
@RequestMapping("test")
public class TestController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Resource
    private FlowService flowService;
    @Resource
    private ActionService actionService;
    @Resource
    private NodeArrowService nodeArrowService;

    @Resource
    private NodeService nodeService;

    @PostMapping("create")
    public Flow create(FlowVo flowVo) {
        Flow flow = new Flow();
        flowVo.setId(UUIDUtil.generatId());
        flowVo.setCurrentNodeId(nodeService.queryByFirstNode(flowVo.getRuleId()).getId());
        BeanUtils.copyProperties(flowVo,flow);
        flowService.insert(flow);
        return startFlow(flow,flowVo.getHandleParams());
    }


    @PostMapping("action")
    public Flow action(ActionVo actionVo) {
        Action action = new Action();
        BeanUtils.copyProperties(actionVo, action);
        action.setId(UUIDUtil.generatId());
        action.setCreateTime(DateUtil.getCurrentDate());
        Flow flow = flowService.queryById(action.getFlowId());
        action.setNodeId(flow.getCurrentNodeId());
        actionService.insert(action);
        return runFlow(flow,action);
    }

    @PostMapping("getCurrentNode")
    public Node getCurrentNode(String id) {
        Flow flow = flowService.queryById(id);
        if (flow == null){
            throw new CustomException(ReturnCode.FLOW_NOT_EXSIT);
        }
        Node node = nodeService.queryById(flow.getCurrentNodeId());
        return node;
    }

    @PostMapping("getHandlePersonList")
    public Node getHandlePersonList(String id) {
        Flow flow = flowService.queryById(id);
        Node node = nodeService.queryById(flow.getCurrentNodeId());
        String condition = node.getHandleUserCondition();
        if (condition.equals("/")){

        } else {

        }
        return node;
    }



    private Flow runFlow(Flow flow, Action action) {
        List<NodeArrow> nodeArrows = nodeArrowService.queryByNoteId(flow.getCurrentNodeId());
        String nextNodeId = null;
        for (NodeArrow item :
                nodeArrows) {
            String condition = fillConditionWithParams(item.getConditionStr(),action.getHandleParams());
            logger.info("condition : [{}]",condition);
            if (ScriptEngineManagerUtil.eval(condition)) {
                nextNodeId = item.getNextNodeId();
                break;
            }
        }
        if (nextNodeId == null){
            Node node = nodeService.queryById(flow.getCurrentNodeId());
            if (node.getType() == -1){
                throw new CustomException(ReturnCode.FLOW_ERROR_ALREADY_FINISHED);
            } else {
                throw new CustomException(ReturnCode.FLOW_ERROR_NO_NEXT_NODE);
            }
        }
        flow.setCurrentNodeId(nextNodeId);
        flowService.update(flow);
        return flow;
    }

    private String fillConditionWithParams(String condition, String params){
        Map<String,Object> map = JSON.parseObject(params,Map.class);
        Iterator<Map.Entry<String, Object>> entries = map.entrySet().iterator();
        while(entries.hasNext()){
            Map.Entry<String, Object> entry = entries.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            System.out.println(key+":"+value);
            condition = condition.replaceAll(key,value.toString());
        }
        System.out.println(condition);
        return condition;
    }


    private Flow startFlow(Flow flow,String handleParams) {
        Action action = new Action();
        action.setId(UUIDUtil.generatId());
        action.setFlowId(flow.getId());
        action.setNodeId(flow.getCurrentNodeId());
        action.setHandlePersonId(flow.getHandlePersonId());
        action.setHandleParams(handleParams);
        action.setCreateTime(DateUtil.getCurrentDate());

        Flow newFlow = runFlow(flow,action);
        actionService.insert(action);
        return newFlow;
    }



}