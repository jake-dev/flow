-- we don't know how to generate schema demo (class Schema) :(
create table if not exists Action
(
	id varchar(32) not null
		primary key,
	flow_id varchar(32) null,
	node_id varchar(32) null,
	handle_person_id varchar(32) null,
	handle_params varchar(2000) null,
	create_time varchar(19) null
)
;

create table if not exists Flow
(
	id varchar(32) not null
		primary key,
	rule_id varchar(32) null,
	name varchar(200) null,
	handle_person_id varchar(32) null,
	current_node_id varchar(32) null,
	create_user_id varchar(32) null,
	create_user_name varchar(200) null
)
;

create table if not exists Node
(
	id varchar(32) not null,
	rule_id varchar(32) null,
	name varchar(200) default '' null,
	type int default '1' null comment '0 开始 1 进行中 -1 归档',
	handle_user_condition varchar(200) null,
	constraint Node_id_uindex
		unique (id)
)
;

alter table Node
	add primary key (id)
;

create table if not exists Node_Arrow
(
	id varchar(32) not null
		primary key,
	node_id varchar(32) null,
	mark varchar(32) null,
	name varchar(200) null,
	condition_str varchar(2000) null,
	next_node_id varchar(32) null,
	rule_id varchar(32) null
)
;

create table if not exists Rule
(
	id varchar(32) not null,
	name varchar(200) default '' not null,
	constraint Rule_id_uindex
		unique (id)
)
;

alter table Rule
	add primary key (id)
;



INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('188643e8922846179e2755b7edec680c', 'fcd25a04fdf14a78aaa65b6115e40347', '1', 'zhangsan111', '{day:3}', '2019-12-01 09:06:02');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('1ff74c8112164dafa657ae43c05120d9', '6a40220605b6494993b1b2af0577f534', '3', 'zhangsan111', '{approve:true}', '2019-12-01 09:16:09');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('2b9d504a0e0445498a117cbdc575299d', '0c4a2d7592894eba8f45b5b7daa1a2bd', '4', 'zhangsan111', '{approve:true}', '2019-12-01 09:12:03');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('2e64e6bcef264bd88f24ab1edafce6ca', '6a40220605b6494993b1b2af0577f534', '1', 'zhangsan111', '{day:1}', '2019-12-01 09:15:56');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('425d0d30fec14504b37eb406080275da', '6a40220605b6494993b1b2af0577f534', '4', 'zhangsan111', '{approve:true}', '2019-12-01 09:16:13');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('45b68abe4c5146eb92670b2ccdf7d1ff', '6a40220605b6494993b1b2af0577f534', '4', 'zhangsan111', '{approve:true}111', '2019-12-01 09:16:22');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('4803cf142e1c4f8a8d91acd9f8d3dd31', '0c4a2d7592894eba8f45b5b7daa1a2bd', '2', 'zhangsan111', '{approve:true}', '2019-12-01 09:11:56');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('4f12eb222d3a42bdb9c12b637a08c58d', '7005c95940cc453b80ab9239f25530ab', '1', 'zhangsan111', '{day:1}', '2019-12-01 09:16:30');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('717ba7d0b72647a488de421405bb6ceb', '6a40220605b6494993b1b2af0577f534', '4', 'zhangsan111', '{approve:true}', '2019-12-01 09:16:13');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('79525437be854694870fac1728e3c1cf', '6a40220605b6494993b1b2af0577f534', '4', 'zhangsan111', '{approve:true}', '2019-12-01 09:16:12');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('87dd9dc063614538a631e9cfddb8b1dd', '7005c95940cc453b80ab9239f25530ab', '2', 'zhangsan111', '{approve:tr111ue}', '2019-12-01 09:16:37');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('945fc5ff78fd4893be4e1e27f9c70e47', '6a40220605b6494993b1b2af0577f534', '2', 'zhangsan111', '{approve:true}', '2019-12-01 09:16:08');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('949eb0e71d974984984702fbd3d7e0a5', '0c4a2d7592894eba8f45b5b7daa1a2bd', '4', 'zhangsan111', '{approve:true}', '2019-12-01 09:16:01');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('caf83bb98f1e45ce87fbd43e75377594', '6a40220605b6494993b1b2af0577f534', '4', 'zhangsan111', '{approve:true}111', '2019-12-01 09:16:21');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('d5ad0809333948bfa00a89c9a9c0012c', '6a40220605b6494993b1b2af0577f534', '4', 'zhangsan111', '{approve:true}', '2019-12-01 09:16:09');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('e8a9ebfbe9db4d4c936138efa6379d89', '0c4a2d7592894eba8f45b5b7daa1a2bd', '3', 'zhangsan111', '{approve:true}', '2019-12-01 09:12:02');
INSERT INTO demo.Action (id, flow_id, node_id, handle_person_id, handle_params, create_time) VALUES ('f33f1eff7fc94c57a66960938572d8cd', '0c4a2d7592894eba8f45b5b7daa1a2bd', '1', 'zhangsan111', '{day:1}', '2019-12-01 09:11:41');
INSERT INTO demo.Flow (id, rule_id, name, handle_person_id, current_node_id, create_user_id, create_user_name) VALUES ('0c4a2d7592894eba8f45b5b7daa1a2bd', '1', '测试流程2', 'zhangsan111', '4', null, null);
INSERT INTO demo.Flow (id, rule_id, name, handle_person_id, current_node_id, create_user_id, create_user_name) VALUES ('6a40220605b6494993b1b2af0577f534', '1', '测试流程2', 'zhangsan111', '4', null, null);
INSERT INTO demo.Flow (id, rule_id, name, handle_person_id, current_node_id, create_user_id, create_user_name) VALUES ('7005c95940cc453b80ab9239f25530ab', '1', '测试流程2', 'zhangsan111', '2', null, null);
INSERT INTO demo.Flow (id, rule_id, name, handle_person_id, current_node_id, create_user_id, create_user_name) VALUES ('fcd25a04fdf14a78aaa65b6115e40347', '1', '测试流程2', 'zhangsan111', '3', null, null);
INSERT INTO demo.Node (id, rule_id, name, type, handle_user_condition) VALUES ('1', '1', '草稿', 0, '{}');
INSERT INTO demo.Node (id, rule_id, name, type, handle_user_condition) VALUES ('2', '1', '主管审批', 1, '{deptCode:111,roleId:111}');
INSERT INTO demo.Node (id, rule_id, name, type, handle_user_condition) VALUES ('3', '1', '领导审批', 1, '{deptCode:222,roleId:222}');
INSERT INTO demo.Node (id, rule_id, name, type, handle_user_condition) VALUES ('4', '1', '归档', -1, '{}');
INSERT INTO demo.Node_Arrow (id, node_id, mark, name, condition_str, next_node_id, rule_id) VALUES ('1', '1', '1_to_lingdao', '提交', 'day >= 3', '3', '1');
INSERT INTO demo.Node_Arrow (id, node_id, mark, name, condition_str, next_node_id, rule_id) VALUES ('2', '1', '1_to_zhuguan', '提交', 'day < 3', '2', '1');
INSERT INTO demo.Node_Arrow (id, node_id, mark, name, condition_str, next_node_id, rule_id) VALUES ('3', '2', '2_to_caogao', '主管不同意', 'approve == false', '1', '1');
INSERT INTO demo.Node_Arrow (id, node_id, mark, name, condition_str, next_node_id, rule_id) VALUES ('4', '2', '2_to_lingdao', '主管同意', 'approve == true', '3', '1');
INSERT INTO demo.Node_Arrow (id, node_id, mark, name, condition_str, next_node_id, rule_id) VALUES ('5', '3', '3_to_caogao', '领导不同意', 'approve == false', '1', '1');
INSERT INTO demo.Node_Arrow (id, node_id, mark, name, condition_str, next_node_id, rule_id) VALUES ('6', '3', '3_to_guidang', '领导同意', 'approve == true', '4', '1');
INSERT INTO demo.Rule (id, name) VALUES ('1', '请假流程');
INSERT INTO demo.Rule (id, name) VALUES ('2', '投票流程');