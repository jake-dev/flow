package com.dream.flow.controller;

import com.alibaba.fastjson.JSON;
import com.dream.flow.util.ScriptEngineManagerUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.Map;

class ActionControllerTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void selectOne() {
        System.out.println("111");
        String str = "{xx:89,yy:33}";
        String script="xx>90 && yy == 33";
        Map<String,Object> map = JSON.parseObject(str,Map.class);
        Iterator<Map.Entry<String, Object>> entries = map.entrySet().iterator();
        while(entries.hasNext()){
            Map.Entry<String, Object> entry = entries.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            System.out.println(key+":"+value);
            script = script.replaceAll(key,value.toString());
        }
        System.out.println(script);
        System.out.println(ScriptEngineManagerUtil.eval(script));
    }

}